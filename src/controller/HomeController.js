import { json } from "express";
import connection from "../configs/connectDB";

class HomeController{
    getHomePage(req, res){
        //simple query
        connection.query(
        'SELECT * FROM `user`',
        (err, results, fields) =>{
            console.log(results); // results contains rows returned by server
            //console.log(fields); // fields contains extra meta data about results, if available
            return res.render('index.ejs', {results: JSON.stringify(results)});
        });
    }
}
export default new HomeController