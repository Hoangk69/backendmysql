import express from "express";
import configViewEngine from "./configs/ViewEngine";
import initRouter from "./route/index";
//import connection from "./configs/connectDB";
require('dotenv').config();

const app = express();
const port = process.env.PORT;

// Middleware body-parser
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// viewEngine
configViewEngine(app);

// router
initRouter(app);

app.listen(port, () => {
  console.log(`App listening on http://localhost:${port}`)
})