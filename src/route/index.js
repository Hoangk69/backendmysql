import express from "express";
import HomeController from "../controller/HomeController";
const router = express.Router();

const initRouter = (app) => {
    router.get('/', HomeController.getHomePage)

    return app.use('/', router)
}

export default initRouter